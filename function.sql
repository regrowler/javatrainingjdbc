CREATE OR REPLACE FUNCTION join() RETURNS TABLE
                                          (
                                              vehicle_model_id   INT,
                                              vehicle_type_name  VARCHAR(255),
                                              vehicle_label_name VARCHAR(255),
                                              vehicle_model_name VARCHAR(255)
                                          )
 AS
 $$
   select vehicle_model_id, vehicle_type_name, vehicle_label_name, vehicle_model_name
from testschema.vehicle_model as t1
         left join testschema.vehicle_label as t2
                   on t1.vehicle_model_label_id = t2.vehicle_label_id
         left join testschema.vehicle_type as t3
                   on t2.vehicle_label_type_id = t3.vehicle_type_id

$$ LANGUAGE SQL;