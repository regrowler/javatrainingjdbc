import org.apache.log4j.Logger;

import java.sql.*;
public class DataBaseWorker {
    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    private static final Logger log = Logger.getLogger(DataBaseWorker.class);
    private static final String postgresUrl="";
    private static final String login="postgres";
    private static final String password="123";
    private static final String bdName="testbd";
    private static final String schemaName="testschema";
    public static void prepareDatabase(){
        log.info("preparing db");
        createDb();
        createScheme();
        createTables();
        setTrigger();
        setFunction();
        log.info("finished preparing db");
    }
    public static void showMax(){
        log.info("test of max");
        try (Connection connection= DriverManager.getConnection(postgresUrl+bdName,login,password)){
            if(connection!=null){
                try (Statement statement=connection.createStatement()){
                    ResultSet set=statement.executeQuery("SELECT  max(vehicle_model_id) from "+schemaName+".vehicle_model;");
                    while (set.next()){
                        log.info("test of max:"+set.getString(1));
                    }
                }catch (Exception e){
                    log.error("test of max: "+e.getMessage());
                }
            }
        }catch (SQLException e){
            log.error("test of max: "+e.getMessage());
        }
        log.info("finished test of max");
    }
    public static void showMin(){
        log.info("test of min");
        try (Connection connection= DriverManager.getConnection(postgresUrl+bdName,login,password)){
            if(connection!=null){
                try (Statement statement=connection.createStatement()){
                    ResultSet set=statement.executeQuery("SELECT  min(vehicle_model_id) from "+schemaName+".vehicle_model;");
                    while (set.next()){
                        log.info("test of min:"+set.getString(1));
                    }
                }catch (Exception e){
                    log.error("test of min: "+e.getMessage());
                }
            }
        }catch (SQLException e){
            log.error("test of min: "+e.getMessage());
        }
        log.info("finished test of min");
    }
    public static void editModel(String name,int id){
        log.info("test of set ");
        try (Connection connection= DriverManager.getConnection(postgresUrl+bdName,login,password)){
            if(connection!=null){
                try (PreparedStatement statement=connection.prepareStatement("UPDATE "+schemaName+".vehicle_model set vehicle_model_name=? where vehicle_model_id=?")){
                    statement.setInt(2,id);
                    statement.setString(1,name);
                    statement.executeUpdate();
                    log.info("test of set finished correctly");
                }catch (Exception e){
                    log.error("test of set finished correctly: "+e.getMessage());
                }
            }
        }catch (SQLException e){
            log.error("test of set finished correctly: "+e.getMessage());
        }
    }
    public static void showFunctionWithJoin(){
        log.info("test of function and join ");
        try (Connection connection=DriverManager.getConnection(postgresUrl+bdName,login,password)){
            if(connection!=null){
                String runFunction = "{ ? = call join() }";
                try (Statement statement=connection.createStatement()){
                    ResultSet set=statement.executeQuery("select * from join()");
                    while (set.next()){
                        log.info("test of function and join: "+set.getString(1)+" "+set.getString(2)+" "+set.getString(3)+" "+set.getString(4));
                        //System.out.println(set.getString(1)+" "+set.getString(2)+" "+set.getString(3)+" "+set.getString(4));
                    }
                    int y=0;

                }catch (Exception e){
                    log.error("test of function and join: "+e.getMessage());
                }
            }
        } catch (SQLException e) {
            log.error("test of function and join: "+e.getMessage());
        }
        log.info("test of function and join finished");
    }
    private static void setTrigger(){
        log.info("setting trigger");
        try (Connection connection= DriverManager.getConnection(postgresUrl+bdName,login,password)){
            if(connection!=null){
                try (Statement statement=connection.createStatement()){
                    statement.executeUpdate(ScriptReader.getScript("trigger.sql"));
                }catch (Exception e){
                    log.error("setting trigger: "+e.getMessage());
                }
            }
        }catch (SQLException e){
            log.error("setting trigger: "+e.getMessage());
        }
        log.info("setting trigger finished");
    }
    private static void setFunction(){
        log.info("setting function");
        try (Connection connection= DriverManager.getConnection(postgresUrl+bdName,login,password)){
            if(connection!=null){
                try (Statement statement=connection.createStatement()){
                    statement.executeUpdate(ScriptReader.getScript("function.sql"));
                }catch (Exception e){
                    log.error("setting function: "+e.getMessage());
                }
            }
        }catch (SQLException e){
            log.error("setting function: "+e.getMessage());
        }
        log.info("setting function finished");
    }
    public static void addVehicleType(String name){
        log.info("test add vehice type "+name);
        try (Connection connection= DriverManager.getConnection(postgresUrl+bdName,login,password)){
            if(connection!=null){
                try (Statement statement=connection.createStatement()){
                    statement.executeUpdate("insert into "+schemaName+".vehicle_type (vehicle_type_name) values (\'"+name +"\')");
                }catch (Exception e){
                    log.error("test add vehice type : "+e.getMessage());
                }
            }
        }catch (SQLException e){
            log.error("test add vehice type : "+e.getMessage());
        }
    }
    public static void addVehicleLabel(String name,int id){
        log.info("test add vehice label "+name);
        try (Connection connection= DriverManager.getConnection(postgresUrl+bdName,login,password)){
            if(connection!=null){
                try (PreparedStatement statement=connection.prepareStatement("insert into "+schemaName+".vehicle_label (vehicle_label_type_id,vehicle_label_name) values (?,?)")){
                    statement.setInt(1,id);
                    statement.setString(2,name);
                    statement.executeUpdate();
                }catch (Exception e){
                    log.error("test add vehice label: "+e.getMessage());
                }
            }
        }catch (SQLException e){
            log.error("test add vehice label: "+e.getMessage());
        }
    }
    public static void addVehicleModel(String name,int id){
        log.info("test add vehice model "+name);
        try (Connection connection= DriverManager.getConnection(postgresUrl+bdName,login,password)){
            if(connection!=null){
                try (PreparedStatement statement=connection.prepareStatement("insert into "+schemaName+".vehicle_model (vehicle_model_label_id,vehicle_model_name) values (?,?)")){
                    statement.setInt(1,id);
                    statement.setString(2,name);
                    statement.executeUpdate();
                }catch (Exception e){
                    log.error("test add vehice model: "+e.getMessage());
                }
            }
        }catch (SQLException e){
            log.error("test add vehice model: "+e.getMessage());
        }
    }
    public static void checkTrigger(){
        log.info("test trigger ");
        try (Connection connection= DriverManager.getConnection(postgresUrl+bdName,login,password)){
            if(connection!=null){
                try (Statement statement=connection.createStatement()){
                    statement.executeUpdate("delete from "+schemaName+".vehicle_type where vehicle_type_id=1");
                }catch (Exception e){
                    log.error("test trigger: "+e.getMessage());
                }
            }
        }catch (SQLException e){
            log.error("test trigger: "+e.getMessage());
        }
    }
    private static void createDb(){
        log.info("creating db");
        try (Connection connection= DriverManager.getConnection(postgresUrl,login,password)){
            if(connection!=null){
                try (Statement statement=connection.createStatement()){
                    statement.executeUpdate("create database "+bdName);
                }catch (Exception e){
                    log.error("creating db: "+e.getMessage());
                }
            }
        }catch (SQLException e){
            log.error("creating db: "+e.getMessage());
        }
    }
    private static void createScheme(){
        log.info("creating scheme");
        try (Connection connection= DriverManager.getConnection(postgresUrl+bdName,login,password)){
            if(connection!=null){
                try (Statement statement=connection.createStatement()){
                    statement.executeUpdate("create schema if not exists "+schemaName);
                }catch (Exception e){
                    log.error("creating scheme: "+e.getMessage());
                }
            }
        }catch (SQLException e){
            log.error("creating scheme: "+e.getMessage());
        }
    }
    private static void createTables(){
        log.info("creating tales");
        try (Connection connection= DriverManager.getConnection(postgresUrl+bdName,login,password)){
            if(connection!=null){
                connection.setAutoCommit(false);
                try (Statement statement=connection.createStatement()){
                    statement.execute("create table if not exists "+schemaName+".vehicle_type\n" +
                            "            (\n" +
                            "    vehicle_type_id serial not null\n" +
                            "    constraint vehicle_type_pk\n" +
                            "    primary key,\n" +
                            "\t\"vehicle_type_name\" varchar(255) not null\n" +
                            "            );");
                    statement.execute("create table if not exists "+schemaName+".vehicle_label\n" +
                            "(\n" +
                            "\tvehicle_label_id serial not null\n" +
                            "\t\tconstraint vehicle_label_pk\n" +
                            "\t\t\tprimary key,\n" +
                            "\tvehicle_label_type_id int,\n" +
                            "\tvehicle_label_name varchar(255)\n" +
                            ");\n" +
                            "\n");
                    statement.execute("create table if not exists "+schemaName+".vehicle_model\n" +
                            "(\n" +
                            "\tvehicle_model_id serial not null\n" +
                            "\t\tconstraint vehicle_model_pk\n" +
                            "\t\t\tprimary key,\n" +
                            "\tvehicle_model_label_id int not null,\n" +
                            "\tvehicle_model_name varchar(255)\n" +
                            ");\n" +
                            "\n");
                    connection.commit();
                }catch (Exception e){
                    log.error("creating tales: "+e.getMessage());
                }
            }
        }catch (SQLException e){
            log.error("creating tales: "+e.getMessage());
        }
    }

    public static void cleanUp(){
        log.info("cleaning up");
        try (Connection connection= DriverManager.getConnection(postgresUrl,login,password)){
            if(connection!=null){
                try (Statement statement=connection.createStatement()){
                    statement.executeUpdate("drop database "+bdName);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }catch (SQLException e){
            log.error("cleaning up: "+e.getMessage());
//            e.printStackTrace();
        }
    }
}
