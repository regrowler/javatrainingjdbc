import java.io.*;

public class ScriptReader  {
    public static String getScript(String name){
        try (BufferedReader reader=new BufferedReader(new FileReader(name))){
            StringBuilder builder=new StringBuilder();
            reader.lines().forEach(s ->  {builder.append(s);builder.append("\n");});
            return builder.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
