

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.runner.RunWith;

import java.sql.SQLException;
import java.util.stream.Stream;

public class Test {

    @BeforeClass
    public static void init(){
        DataBaseWorker.prepareDatabase();
        Stream.of("car", "plane", "bike").forEach(s -> DataBaseWorker.addVehicleType(s));
        Stream.of("mercedes-benz", "mazda", "lada").forEach(s -> DataBaseWorker.addVehicleLabel(s, 1));
        Stream.of("cesna", "piper", "IL").forEach(s -> DataBaseWorker.addVehicleLabel(s, 2));
        Stream.of("harley", "kawasaki", "bmw").forEach(s -> DataBaseWorker.addVehicleLabel(s, 3));
        DataBaseWorker.addVehicleModel("ninja",8);
        DataBaseWorker.addVehicleModel("kalina",3);
        DataBaseWorker.addVehicleModel("1200 Gs",9);
        DataBaseWorker.addVehicleModel("190e",1);
        DataBaseWorker.addVehicleModel("6 mps",2);
        DataBaseWorker.addVehicleModel("182",4);
        DataBaseWorker.addVehicleModel("aztec",5);
        DataBaseWorker.addVehicleModel("2 StormTrooper",6);
        DataBaseWorker.addVehicleModel("davidson",7);
    }
    @AfterClass
    public static void drop(){
        DataBaseWorker.cleanUp();
    }

    @org.junit.Test()
    public void trigger() {
        DataBaseWorker.checkTrigger();
    }

    @org.junit.Test
    public void all() {
        DataBaseWorker.editModel("pa23",7);
        DataBaseWorker.showMax();
        DataBaseWorker.showMin();
        DataBaseWorker.showFunctionWithJoin();
    }
}
