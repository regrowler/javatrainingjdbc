CREATE FUNCTION check_del_cat() RETURNS trigger AS $check_del_cat$
BEGIN
    IF OLD.vehicle_type_id = 1 /*substitute primary key value for your row*/ THEN
        RAISE EXCEPTION 'cannot delete default category';
    END IF;

END;
$check_del_cat$ LANGUAGE plpgsql;

CREATE TRIGGER check_del_cat BEFORE DELETE ON testschema.vehicle_type/*table name*/
    FOR EACH ROW EXECUTE PROCEDURE check_del_cat();